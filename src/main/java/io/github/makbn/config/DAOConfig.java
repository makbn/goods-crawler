package io.github.makbn.config;

import io.github.makbn.dao.RoleDAOImp;
import io.github.makbn.dao.UserDAOImp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class DAOConfig {


    @Bean
    public UserDAOImp getUserDao(){
        UserDAOImp userDAOImp =new UserDAOImp();
        return userDAOImp;
    }

    @Bean
    public RoleDAOImp getRoleDAO(){
        RoleDAOImp roleDAOImp =new RoleDAOImp();

        return roleDAOImp;
    }

}
