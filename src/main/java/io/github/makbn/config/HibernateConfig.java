package io.github.makbn.config;

import io.github.makbn.domain.Role;
import io.github.makbn.domain.User;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScans(value = { @ComponentScan(HibernateConfig.DOMAIN_PACKAGE)})
@EnableAutoConfiguration(exclude=HibernateJpaAutoConfiguration.class)
public class HibernateConfig {
    public final static String DOMAIN_PACKAGE="io.github.makbn.domain";
    private final static String DB_NAME="test";
    private final static String DB_PORT="3306";
    private final static String DB_HOST="127.0.0.1";
    private final static String DB_USERNAME="root";
    private final static String DB_PASSWORD="12345678";


    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setHibernateProperties(hibernateProperties());
        sessionFactory.setAnnotatedClasses(User.class, Role.class);
        return sessionFactory;
    }

    @Bean
    @Primary
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl(getUrl());
        dataSource.setUsername(DB_USERNAME);
        dataSource.setPassword(DB_PASSWORD);

        return dataSource;
    }

    @Bean
    public PlatformTransactionManager hibernateTransactionManager() {
        HibernateTransactionManager transactionManager
                = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }

    private final Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty(
                "hibernate.hbm2ddl.auto", "create-drop");
        hibernateProperties.setProperty(
                "hibernate.dialect","org.hibernate.dialect.MySQL5Dialect");
        return hibernateProperties;
    }

    private final String getUrl(){
        return "jdbc:mysql://{DB_HOST}:{DB_PORT}/{DB_NAME}".replace("{DB_HOST}",DB_HOST)
                .replace("{DB_PORT}",DB_PORT)
                .replace("{DB_NAME}",DB_NAME);
    }
}
