package io.github.makbn.dao;

import io.github.makbn.domain.User;

public interface UserDAO extends BaseDAO<User> {

    User findUserByEmail(String email);
}
