package io.github.makbn.dao;

import io.github.makbn.domain.Role;

public interface RoleDAO extends BaseDAO<Role> {

    Role findByRole(String role);

}
