package io.github.makbn.dao;

public interface BaseDAO<T> {



    T findEntity(int id);

    void createEntity(T entity);

    void deleteEntity(int id);

    void updateEntity(T entity);

}
